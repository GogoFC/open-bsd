# Security Updates

Apply binary patches (available on amd64, arm64, i386)

The [syspatch(8)](https://man.openbsd.org/syspatch) utility can be used to upgrade any files in need of security or reliability fixes on a supported OpenBSD release. This is the quickest and easiest method to get the base system up to date.

Run:

```sh
syspatch
```

### Installing patches

![ss](images/syspatch.png)

After it finishes `reboot` the system.


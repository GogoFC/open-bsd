# Virtual Box

### Set up a new VM on Virtual Box

Summary:

- Select to create a New Virtual Machine
- Select 2048 MB of RAM if running GNOME. 1024 MB is enough if GNOME will not be run.
- Select 50 GB of Disk space if you will install GNOME, otherwise 30 GB is enough.
- Select either 1 or 2 CPU. 1 if you run into problems after install.
- Select to increase Video memory if you wish.
- Select NAT for Network, or other prefered network.
- Attach boot volume now or later when you start machine. It will ask for it if not attached
- After reboot or shutdown and detach the DVD ISO file in Virtual Box and start OS to boot from the disk.

### Video

[https://youtu.be/4e7AF8HqZfo](https://youtu.be/4e7AF8HqZfo)

# Screenshots

### Create New Virtual Machine

![vbox](images/0vbox.png)

### Naming the VM

![vbox](images/1vbox.png)

### Select Memory Size

![vbox](images/2vbox.png)

### Create a new Hard Disk


![vbox](images/3vbox.png)

### Hard Disk Type

![vbox](images/4vbox.png)

### Disk of fixed-size is faster

![vbox](images/5vbox.png)

### Choose Hard Disk size

![vbox](images/6vbox.png)

### Disk Being Created

![vbox](images/7vbox.png)

### Select Number of CPU

![vbox](images/8vbox.png)

### Select Video Memory

![vbox](images/9vbox.png)

### Add Optical Drive for booting

![vbox](images/10vbox.png)

![vbox](images/11vbox.png)

### Select boot image 

![vbox](images/12vbox.png)

### Boot image attached

![vbox](images/13vbox.png)

### Select Network

![vbox](images/14vbox.png)

### Start new VM

![vbox](images/15vbox.png)

### Select startup boot disk

![vbox](images/16vbox.png)

### Machine Starting

![vbox](images/17vbox.png)

### Removing a VM

![vbox](images/18vbox.png)

### Delete all VM files

![vbox](images/19vbox.png)


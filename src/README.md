# Install OpenBSD on VirtualBox

Assumptions:

- Using `root` for installing packages.
- Disabling SSH root logins.
- Creating a regular user.

Some Specifics:

- Using `https://mirror.aarnet.edu.au` mirror during install
- Setting Time Zone as `Australia/Brisbane`

### Contents:

- [Getting OpenBSD](./isotousb.md)
- [Virtual Box](./virtualbox.md)
- [Install OpenBSD](./install.md)
- [Post Install](./postinstall.md)
- [Update Packages](./update.md)
- [Upgrade OpenBSD](./upgrade.md)
- [Gnome](./gnome.md)
- [Firefox ESR](./firefox.md)
- [ProtonVPN](./protonvpn.md)
